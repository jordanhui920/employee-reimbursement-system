Employee Reimbursement System.  

A simple system for employees and managers to track, store, and apply reimbursements to employees. Employees can make an account and apply for reimbursements. Managers can then view those requests and choose to approve or deny them.  
  
To start the project, import .project into the Eclipse IDE. Tomcat 9.0 needs to be installed on the IDE. Then you can run as a server on eclipse. A postgresql database needs to be set up to use a database. Include a connection.properties file into the src/main/resources folder with corresponding information to access features related to each. An example of a connection.properties file is listed below with all fields that are asked for.  
url = jdbc:postgresql://databaseurl.com:5432/databasename (5432 being the port number)  
username = database_username (this user should have access to all permissions on said database)  
password = password_for_database_username  
senderEmail = email@domain (to send emails to employees, a valid email and password with smtp permissions is to be supplied)  
emailPassword = emailpassword  
testurl = jdbc:postgresql://testdatabaseurl.com:5432/testdatabasename (Testing is done on a separate database from production)  
In addition to the above, one needs to run all the functions in postgredb.sql in the postegresql database to set up the tables needed and supply the stored functions used in this application.  
For logging to work, one needs to supply a log4j.properties file in the src/main/resources folder.  
  
After the above has been set up, one can host the server and can access the application on localhost:8080/index.html. Or if hosting on a hosting server, serverurl/index.html. The only way to make a new manager is through sql commands. There is one manager with username "admin" and a unhashed password of "1" supplied in the given .sql file. More managers can be made through a database admin. 