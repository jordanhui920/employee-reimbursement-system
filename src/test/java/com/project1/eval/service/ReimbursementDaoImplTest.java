package com.project1.eval.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.project1.dao.DBConnection;
import com.project1.dao.ReimbursementDaoImpl;
import com.project1.dao.UserDaoImpl;
import com.project1.model.Reimbursement;
import com.project1.model.ReimbursementType;
import com.project1.model.User;

public class ReimbursementDaoImplTest {
	String rs; //random string
	UserDaoImpl userDaoImpl;
	ReimbursementDaoImpl reimbursementDaoImpl;
	
	@BeforeEach
	public void setUp() throws Exception{
		rs = generateRandomAlphaNumericString(25,50);
		DBConnection testServer = new DBConnection(true);
		userDaoImpl = new UserDaoImpl(testServer);
		reimbursementDaoImpl = new ReimbursementDaoImpl(testServer);
	}
	
	@Test //attempt login with gibberish, should return nothing
	public void verifyGibberishLoginFail() {
		User retUser = userDaoImpl.verifyLogin(rs,rs);
		assertNull(retUser);
	}
	
	@Test //add 1 unique user, verify login returns it
	public void addNewUserTestVerifyWorks() {
		User user = userDaoImpl.makeNewUser(rs, rs, rs, rs, rs);
		User retUser = userDaoImpl.verifyLogin(rs,rs);
		assertTrue(user.equals(retUser));
	}
	
	@Test //add same unique user twice, assert 2nd attempt returns null
	public void addTwoUser2ndAttemptReturnsNull() {
		userDaoImpl.makeNewUser(rs, rs, rs, rs, rs);
		User user2 = userDaoImpl.makeNewUser(rs, rs, rs, rs, rs);
		assertNull(user2);
	}
	
	@Test //adds a user, then adds a reimbursement to user, asserts reimbursement size increased by 1.
	public void add1ReimbursementAssertTotalReimbursementIncreasedBy1() {
		User user = userDaoImpl.makeNewUser(rs, rs, rs, rs, rs);
		assertNotNull(user);
		List<Reimbursement> holder = reimbursementDaoImpl.getPendingReimbursements();
		int sizeBefore = holder.size();
		int rn = generateRandomPositiveNumberWithMax(Integer.MAX_VALUE - 1);
		int randomType = generateRandomPositiveNumberWithMax(4);
		reimbursementDaoImpl.reimbursementRequest(rn, rs, null, user, new ReimbursementType(randomType));
		holder = reimbursementDaoImpl.getPendingReimbursements();
		int sizeAfter = holder.size();
		assertEquals(sizeBefore, sizeAfter - 1);
	}
	
	@Test //adds a user, then adds 2 same reimbursements to user, asserts reimbursement size increased by 2 since we allow duplicates
	public void add2ReimbursementAssertTotalReimbursementIncreasedBy2() {
		User user = userDaoImpl.makeNewUser(rs, rs, rs, rs, rs);
		List<Reimbursement> holder = reimbursementDaoImpl.getPendingReimbursements();
		int sizeBefore = holder.size();
		int rn = generateRandomPositiveNumberWithMax(Integer.MAX_VALUE - 1);
		int randomType = generateRandomPositiveNumberWithMax(4);
		reimbursementDaoImpl.reimbursementRequest(rn, rs, null, user, new ReimbursementType(randomType));
		reimbursementDaoImpl.reimbursementRequest(rn, rs, null, user, new ReimbursementType(randomType));
		holder = reimbursementDaoImpl.getPendingReimbursements();
		int sizeAfter = holder.size();
		assertEquals(sizeBefore, sizeAfter - 2);
	}
	
    private static String generateRandomAlphaNumericString(int min, int max) {
        int leftLimit = 48; // number 0
        int rightLimit = 122; // letter 'z'
        int targetStringLength = ThreadLocalRandom.current().nextInt(min, max + 1);
        Random random = new Random();

        String generatedString = random.ints(leftLimit, rightLimit + 1)
            .limit(targetStringLength)
            .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
            .toString();

        return generatedString;
    }
	
    private static int generateRandomPositiveNumberWithMax(int max) {
        return ThreadLocalRandom.current().nextInt(0, max + 1);
    }
}
