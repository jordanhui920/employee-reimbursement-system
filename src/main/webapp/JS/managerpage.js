
var table;
//console.log("managerpage.js loaded");
window.onload = function(){ //this will be initialization logic that runs right as the browser window loads
	table = $('#table');
	table.bootstrapTable({
        data:{}
    });
	document.getElementById('displayAll').addEventListener('click', function(){populateTable("All")});
	document.getElementById('displayPending').addEventListener('click', function(){populateTable("Pending")});
	document.getElementById('displayAccepted').addEventListener('click', function(){populateTable("Approved")});
	document.getElementById('displayDenied').addEventListener('click', function(){populateTable("Denied")});
	sayHello();
	//populateTable();
}

function sayHello(){ 
	let xhttp =  new XMLHttpRequest();
	xhttp.onreadystatechange = function(){
		if(xhttp.readyState==4 && xhttp.status==200){
			let currentUser = JSON.parse(xhttp.responseText);
			let name = `${currentUser.user_first_name} ${currentUser.user_last_name}`;
			document.getElementById("helloName").innerText = `Hello ${name}`;
		}
	}
	xhttp.open('GET', "http://localhost:8080/project1/getsessionuser.json");
	xhttp.send();
}

function populateTable(type){ 
	//console.log("populatetablestart");
	let xhttp =  new XMLHttpRequest();
	xhttp.onreadystatechange = function(){
		if(xhttp.readyState==4 && xhttp.status==200){
			//console.log("start populate");
			let pokeObj;
			try {
				pokeObj = JSON.parse(xhttp.responseText);
			}
			catch (err){
				window.location.href = "index.html";
			}
			//pokeManipulation(pokeObj);
			//console.log(pokeObj);
			//console.log(pokeObj[0]);
			//console.log(JSON.stringify(pokeObj));
			var mydata = pokeObj;
			/*
			{"reimb_id":8,"reimb_amount":1,"reimb_submitted":1634587069242,"reimb_resolved":null,
			"reimb_description":"desc","receipt":null,"reimb_author":"3  4","reimb_resolver":"null null",
			"status":{"status":"Pending"},"type":{"type":"Lodging","intType":1}}
			*/
			$("#inlineFormApprove").empty();
			$("#inlineFormDeny").empty();
			for (var i = 0; i < pokeObj.length; i++) {
			    pokeObj[i].reimb_submitted = new Date(pokeObj[i].reimb_submitted);
				if (pokeObj[i].reimb_resolved)
					pokeObj[i].reimb_resolved = new Date(pokeObj[i].reimb_resolved);
				else
					pokeObj[i].reimb_resolved = "N/A";
				if (pokeObj[i].receipt)
					pokeObj[i].receipt = "<img src=" + "data:image;base64,"+unescape(pokeObj[i].receipt) +"></img>";
				else
					pokeObj[i].receipt = "N/A";
				if (pokeObj[i].reimb_resolver == "null null")
					pokeObj[i].reimb_resolver = "N/A";
				pokeObj[i].status = pokeObj[i].status.status;
				pokeObj[i].type = pokeObj[i].type.type;
				if (pokeObj[i].status == "Denied") {
					pokeObj[i].status = `<a style="background-color: red">Denied</a>`;
				}
				else if (pokeObj[i].status == "Approved") {
					pokeObj[i].status = `<a style="background-color: green">Approved</a>`;
				}
				else {
					pokeObj[i].status = `<a style="background-color: yellow">Pending</a>`;
					$("#inlineFormApprove").append(`<option value="${pokeObj[i].reimb_id}">${pokeObj[i].reimb_id}</option>`);
					$("#inlineFormDeny").append(`<option value="${pokeObj[i].reimb_id}">${pokeObj[i].reimb_id}</option>`);
					//inlineFormApprove
				}
			}
		    table.bootstrapTable('destroy');
		    table.bootstrapTable({
		        data: mydata
		    });
		}
	}
	if (type == "All")
		xhttp.open('GET', "http://localhost:8080/project1/getReimbursementRequest.json");
	else if (type == "Pending")
		xhttp.open('GET', "http://localhost:8080/project1/getPendingReimbursements.json");
	else if (type == "Approved")
		xhttp.open('GET', "http://localhost:8080/project1/getAcceptedReimbursements.json");
	else if (type == "Denied")
		xhttp.open('GET', "http://localhost:8080/project1/getDeniedReimbursements.json");
	xhttp.send();
}

function pokeManipulation(pokeObj){ // once we get the response with the Pokemon object we will call this function to change the html page
	document.getElementById('pokeName').innerText = `Name: ${pokeObj.name}`;
	document.getElementById('pokeType').innerText = `Type: ${pokeObj.types[0].type.name}`;
	document.getElementById('pokeImage').setAttribute("src", pokeObj.sprites.front_shiny);
}
