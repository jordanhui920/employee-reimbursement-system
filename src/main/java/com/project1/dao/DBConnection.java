package com.project1.dao;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.log4j.Logger;

public class DBConnection {
    ClassLoader classloader = getClass().getClassLoader();
    InputStream is;
    Properties p = new Properties();
    boolean testServer = false;
    private static Logger log = Logger.getLogger(DBConnection.class);

    public DBConnection() {
    	log.debug("created new DB Connection");
        is = classloader.getResourceAsStream("connection.properties");
        try {
            Class.forName("org.postgresql.Driver");
            p.load(is);
        } catch (IOException e) {
        	log.error(e);
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
        	log.error(e);
            e.printStackTrace();
        }
    }

    public DBConnection(boolean testServer) {
    	log.debug("created new DB Connection with test server");
    	this.testServer = testServer;
        is = classloader.getResourceAsStream("connection.properties");
        try {
            Class.forName("org.postgresql.Driver");
            p.load(is);
        } catch (IOException e) {
        	log.error(e);
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
        	log.error(e);
            e.printStackTrace();
        }
    }

    public Connection getDBConnection() throws SQLException {
    	log.debug("attempted connection with db server");
    	String URL;
    	if (this.testServer)
    		URL = p.getProperty("testurl");
    	else
    		URL = p.getProperty("url");
        final String USERNAME = p.getProperty("username");
        final String PASSWORD = p.getProperty("password");
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
        	log.error(e);
            e.printStackTrace();
        }
        return DriverManager.getConnection(URL, USERNAME, PASSWORD);
    }
}