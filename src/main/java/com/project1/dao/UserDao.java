package com.project1.dao;

import java.util.List;

import com.project1.model.Reimbursement;
import com.project1.model.User;

public interface UserDao {
	public User makeNewUser(String username, String password, String fname, String lname, String email); //defaults to employee
	public User verifyLogin(String username, String password);
	public List<Reimbursement> getAllUserRequests(User user);
	public List<Reimbursement> getAllUserRequests();
}
