package com.project1.dao;

import java.security.MessageDigest;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import com.project1.model.Reimbursement;
import com.project1.model.ReimbursementStatus;
import com.project1.model.ReimbursementType;
import com.project1.model.User;
import com.project1.model.UserRole;

public class UserDaoImpl implements UserDao {

    private DBConnection connection;
    private static Logger log = Logger.getLogger(UserDaoImpl.class);

    public UserDaoImpl() {
        connection = new DBConnection();
    }
    
    public UserDaoImpl(DBConnection con) {
        connection = con;
    }

    //only able to insert employees
    public User makeNewUser(String username, String password, String fname, String lname, String email) {
    	log.debug("in userdaoimpl makenewuser");
        password = sha256shortened(password);
        User newUser = null;
        try (Connection con = connection.getDBConnection()) {
            String sql = "{?= call insert_new_user(?,?,?,?,?)}";
            CallableStatement cs = con.prepareCall(sql);
            cs.registerOutParameter(1, Types.INTEGER);
            cs.setString(2, username);
            cs.setString(3, password);
            cs.setString(4, fname);
            cs.setString(5, lname);
            cs.setString(6, email);
            cs.execute();
            int id = cs.getInt(1);
            if (id > 0) {
                log.info("Created new user with id of " + id + " and username " + username);
                newUser = new User(id, username, password, fname, lname, email, new UserRole("Employee"));
            } else {
                log.info("Failed to create new user with username " + username);
            }
        } catch (SQLException e) {
            log.error(e);
            e.printStackTrace();
        }
        return newUser;
    }

    public User verifyLogin(String username, String password) {
    	log.debug("in userdaoimpl verifylogin");
        password = sha256shortened(password);
        User logUser = null;
        try (Connection con = connection.getDBConnection()) {
            String sql = "{call verify_login(?,?)}";
            CallableStatement cs = con.prepareCall(sql);
            cs.setString(1, username);
            cs.setString(2, password);
            ResultSet rs = cs.executeQuery();
            List < User > users = new LinkedList < > ();
            while (rs.next()) {
                users.add(new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4),
                    rs.getString(5), rs.getString(6), new UserRole(rs.getString(7))));
            }
            if (users.size() == 0) { // no login found
                log.info("failed login attempt on username " + username);
            } else if (users.size() == 1) { //set the logUser to the user and return
                log.info("successful login on username " + username);
                logUser = users.get(0);
            } else { //fatal error, should never happen, why is username not unique?
            	log.fatal("multiple users with username " + username + " found");
            }
        } catch (SQLException e) {
            log.error(e);
            e.printStackTrace();
        }
        return logUser;
    }

    public List < Reimbursement > getAllUserRequests(User user) {
    	log.debug("in userdaoimpl getalluserrequests personaluser");
        List < Reimbursement > reimbursements = new LinkedList < > ();
        try (Connection con = connection.getDBConnection()) {
            String sql = "{call get_all_user_requests(?)}";
            CallableStatement cs = con.prepareCall(sql);
            cs.setInt(1, user.getErs_users_id());
            ResultSet rs = cs.executeQuery();
            log.info("called get_all_user_requests for user " + user.getErs_users_id());
            while (rs.next()) {
                /*
			(int reimb_id, int reimb_amount, Timestamp reimb_submitted, Timestamp reimb_resolved,
			String reimb_description, byte[] receipt, String reimb_author, String reimb_resolver,
			ReimbursementStatus status, ReimbursementType type //new reimbursement
				 */
                /*
                id int, reimb_amount int, reimb_submitted timestamp, reimb_resolved timestamp, 
                reimb_description varchar(250), reimb_receipt bytea, reimb_resolver_fname varchar(100), 
                reimb_resolver_lname varchar(100), reimb_status_id varchar(10), reimb_type varchar(10)
                 */
                reimbursements.add(new Reimbursement(rs.getInt(1), rs.getInt(2),
                    rs.getTimestamp(3), rs.getTimestamp(4), rs.getString(5), rs.getBytes(6),
                    user.getFullName(), rs.getString(7) + " " + rs.getString(8),
                    new ReimbursementStatus(rs.getString(9)),
                    new ReimbursementType(rs.getString(10))));
                //accounts.add(new Account(rs.getInt(1), rs.getInt(2)));
            }
        } catch (SQLException e) {
            log.error(e);
            e.printStackTrace();
        }
        return reimbursements;
    }


    public List < Reimbursement > getAllUserRequests(int i) {
        User temp = new User();
        temp.setErs_users_id(i);
        return getAllUserRequests(temp);
    }

    public List < Reimbursement > getAllUserRequests() {
    	log.debug("in userdaoimpl getalluserrequests");
        List < Reimbursement > reimbursements = new LinkedList < > ();
        try (Connection con = connection.getDBConnection()) {
            String sql = "{call get_all_user_requests()}";
            CallableStatement cs = con.prepareCall(sql);
            ResultSet rs = cs.executeQuery();
            log.info("called get every reimbursement");
            while (rs.next()) {
                /*
			(int reimb_id, int reimb_amount, Timestamp reimb_submitted, Timestamp reimb_resolved,
			String reimb_description, byte[] receipt, String reimb_author, String reimb_resolver,
			ReimbursementStatus status, ReimbursementType type //new reimbursement
				 */
                /*
                id int, reimb_amount int, reimb_submitted timestamp, reimb_resolved timestamp, 
                reimb_description varchar(250), reimb_receipt bytea, reimb_resolver_fname varchar(100), 
                reimb_resolver_lname varchar(100), reimb_status_id varchar(10), reimb_type varchar(10), 
                reimb_author_fname varchar(100), reimb_author_lname varchar(100)
                 */
                reimbursements.add(new Reimbursement(rs.getInt(1), rs.getInt(2),
                    rs.getTimestamp(3), rs.getTimestamp(4), rs.getString(5), rs.getBytes(6),
                    rs.getString(11) + " " + rs.getString(12), rs.getString(7) + " " + rs.getString(8),
                    new ReimbursementStatus(rs.getString(9)),
                    new ReimbursementType(rs.getString(10))));
                //accounts.add(new Account(rs.getInt(1), rs.getInt(2)));
            }
        } catch (SQLException e) {
            log.error(e);
            e.printStackTrace();
        }
        return reimbursements;
    }

    //returns first 50 characters of sha256 because of database restriction to only be 50 characters
    private static String sha256shortened(String base) {
        try {
        	log.debug("called sha256 algorithm");
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            final byte[] hash = digest.digest(base.getBytes("UTF-8"));
            final StringBuilder hexString = new StringBuilder();
            for (int i = 0; i < hash.length; i++) {
                final String hex = Integer.toHexString(0xff & hash[i]);
                if (hex.length() == 1)
                    hexString.append('0');
                hexString.append(hex);
            }
            return hexString.toString().substring(0, 50);
        } catch (Exception e) {
            log.error(e);
            throw new RuntimeException(e);
        }
    }
}