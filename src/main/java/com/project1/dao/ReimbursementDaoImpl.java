package com.project1.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import com.project1.model.Reimbursement;
import com.project1.model.ReimbursementStatus;
import com.project1.model.ReimbursementType;
import com.project1.model.User;

public class ReimbursementDaoImpl implements ReimbursementDao {

    private DBConnection connection;
    private static Logger log = Logger.getLogger(ReimbursementDaoImpl.class);

    public ReimbursementDaoImpl() {
        connection = new DBConnection();
        log = Logger.getLogger(UserDaoImpl.class);
    }

    public ReimbursementDaoImpl(DBConnection con) {
        connection = con;
        log = Logger.getLogger(UserDaoImpl.class);
    }

    public boolean reimbursementRequest(int amount, String description, byte[] receipt, User user,
        ReimbursementType type) {
    	log.debug("in reimbursementDaoImpl reimbursementrequest");
        try (Connection con = connection.getDBConnection()) {
            String sql = "{? = call reimbursement_request(?, ?, ?, ?, ?)}";
            CallableStatement cs = con.prepareCall(sql);
            cs.registerOutParameter(1, Types.BOOLEAN);
            cs.setInt(2, amount);
            cs.setString(3, description);
            cs.setBytes(4, receipt);
            cs.setInt(5, user.getErs_users_id());
            cs.setInt(6, type.getIntType());
            cs.execute();
            if (cs.getBoolean(1)) {
                log.info("made reimbursement request");
            } else {
                log.info("couldn't make reimbursement request");
            }
            return cs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
            log.error(e);
        }
        return false;
    }

    public boolean acceptReimbursementRequest(Reimbursement reimbursement, User user) {
        // accept_reimbursement_request(p_reimb_req_id int, p_manager_id int)
    	log.debug("in reimbursementDaoImpl acceptreimbursementrequest");
        try (Connection con = connection.getDBConnection()) {
            String sql = "{? = call accept_reimbursement_request(?, ?)}";
            CallableStatement cs = con.prepareCall(sql);
            cs.registerOutParameter(1, Types.BOOLEAN);
            cs.setInt(2, reimbursement.getReimb_id());
            cs.setInt(3, user.getErs_users_id());
            cs.execute();
            if (cs.getBoolean(1)) {
                log.info("successfully accepted reimbursement request");
            } else {
                log.info("didn't make reimbursement request");
            }
            return cs.getBoolean(1);
        } catch (SQLException e) {
        	log.error(e);
            e.printStackTrace();
        }
        return false;
    }

    public boolean denyReimbursementRequest(Reimbursement reimbursement, User user) {
        // deny_reimbursement_request(p_reimb_req_id int, p_manager_id int)
    	log.debug("in reimbursementDaoImpl denyreimbursementrequest");
        try (Connection con = connection.getDBConnection()) {
            String sql = "{? = call deny_reimbursement_request(?, ?)}";
            CallableStatement cs = con.prepareCall(sql);
            cs.registerOutParameter(1, Types.BOOLEAN);
            cs.setInt(2, reimbursement.getReimb_id());
            cs.setInt(3, user.getErs_users_id());
            cs.execute();
            if (cs.getBoolean(1)) {
                log.info("successfully denied reimbursement request");
            } else {
                log.info("couldn't deny reimbursement request");
            }
            return cs.getBoolean(1);
        } catch (SQLException e) {
        	log.error(e);
            e.printStackTrace();
        }
        return false;
    }

    public List < Reimbursement > getPendingReimbursements() {
    	log.debug("in reimbursementDaoImpl getpendingreimbursementrequest");
        // deny_reimbursement_request(p_reimb_req_id int, p_manager_id int)
        List < Reimbursement > result = new LinkedList < > ();
        try (Connection con = connection.getDBConnection()) {
            String sql = "select r.reimb_id, r.reimb_amount, r.reimb_submitted, r.reimb_resolved, r.reimb_description, r.reimb_receipt, u.user_first_name, \r\n" +
                "		u.user_last_name, s.reimb_status, t.reimb_type, a.user_first_name, a.user_last_name  \r\n" +
                "		from ers_reimbursement r\r\n" +
                "		left join ers_users u on r.reimb_resolver = u.ers_users_id\r\n" +
                "		left join ers_users a on r.reimb_author = a.ers_users_id\r\n" +
                "		left join ers_reimbursement_type t on t.reimb_type_id = r.reimb_type_id\r\n" +
                "		left join ers_reimbursement_status s on s.reimb_status_id =r.reimb_status_id\r\n" +
                "		where r.reimb_status_id = 1;";
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            log.info("got pending reimbursements from db");
            while (rs.next()) {
                /*
			int reimb_id, int reimb_amount, Timestamp reimb_submitted, Timestamp reimb_resolved,
			String reimb_description, byte[] receipt, String reimb_author, String reimb_resolver,
			ReimbursementStatus status, ReimbursementType type
				 */
                result.add(new Reimbursement(rs.getInt(1), rs.getInt(2), rs.getTimestamp(3), rs.getTimestamp(4), rs.getString(5), rs.getBytes(6),
                    rs.getString(11) + " " + rs.getString(12), rs.getString(7) + " " + rs.getString(8),
                    new ReimbursementStatus(rs.getString(9)), new ReimbursementType(rs.getString(10))));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            log.error(e);
        }
        return result;
    }

    public List < Reimbursement > getAcceptedReimbursements() {
        // deny_reimbursement_request(p_reimb_req_id int, p_manager_id int)
    	log.debug("in reimbursementDaoImpl getacceptedreimbursementrequest");
        List < Reimbursement > result = new LinkedList < > ();
        try (Connection con = connection.getDBConnection()) {
            String sql = "select r.reimb_id, r.reimb_amount, r.reimb_submitted, r.reimb_resolved, r.reimb_description, r.reimb_receipt, u.user_first_name, \r\n" +
                "		u.user_last_name, s.reimb_status, t.reimb_type, a.user_first_name, a.user_last_name  \r\n" +
                "		from ers_reimbursement r\r\n" +
                "		left join ers_users u on r.reimb_resolver = u.ers_users_id\r\n" +
                "		left join ers_users a on r.reimb_author = a.ers_users_id\r\n" +
                "		left join ers_reimbursement_type t on t.reimb_type_id = r.reimb_type_id\r\n" +
                "		left join ers_reimbursement_status s on s.reimb_status_id =r.reimb_status_id\r\n" +
                "		where r.reimb_status_id = 2;";
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            log.info("requested accepted reimbursements from db");
            while (rs.next()) {
                /*
			int reimb_id, int reimb_amount, Timestamp reimb_submitted, Timestamp reimb_resolved,
			String reimb_description, byte[] receipt, String reimb_author, String reimb_resolver,
			ReimbursementStatus status, ReimbursementType type
				 */
                result.add(new Reimbursement(rs.getInt(1), rs.getInt(2), rs.getTimestamp(3), rs.getTimestamp(4), rs.getString(5), rs.getBytes(6),
                    rs.getString(11) + " " + rs.getString(12), rs.getString(7) + " " + rs.getString(8),
                    new ReimbursementStatus(rs.getString(9)), new ReimbursementType(rs.getString(10))));
            }
        } catch (SQLException e) {
        	log.error(e);
            e.printStackTrace();
        }
        return result;
    }

    public List < Reimbursement > getDeniedReimbursements() {
        // deny_reimbursement_request(p_reimb_req_id int, p_manager_id int)
        List < Reimbursement > result = new LinkedList < > ();
    	log.debug("in reimbursementDaoImpl getdeniedreimbursementrequest");
        try (Connection con = connection.getDBConnection()) {
            String sql = "select r.reimb_id, r.reimb_amount, r.reimb_submitted, r.reimb_resolved, r.reimb_description, r.reimb_receipt, u.user_first_name, \r\n" +
                "		u.user_last_name, s.reimb_status, t.reimb_type, a.user_first_name, a.user_last_name  \r\n" +
                "		from ers_reimbursement r\r\n" +
                "		left join ers_users u on r.reimb_resolver = u.ers_users_id\r\n" +
                "		left join ers_users a on r.reimb_author = a.ers_users_id\r\n" +
                "		left join ers_reimbursement_type t on t.reimb_type_id = r.reimb_type_id\r\n" +
                "		left join ers_reimbursement_status s on s.reimb_status_id =r.reimb_status_id\r\n" +
                "		where r.reimb_status_id = 3;";
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            log.info("requested denied reimbursements from db");
            while (rs.next()) {
                /*
			int reimb_id, int reimb_amount, Timestamp reimb_submitted, Timestamp reimb_resolved,
			String reimb_description, byte[] receipt, String reimb_author, String reimb_resolver,
			ReimbursementStatus status, ReimbursementType type
				 */
                result.add(new Reimbursement(rs.getInt(1), rs.getInt(2), rs.getTimestamp(3), rs.getTimestamp(4), rs.getString(5), rs.getBytes(6),
                    rs.getString(11) + " " + rs.getString(12), rs.getString(7) + " " + rs.getString(8),
                    new ReimbursementStatus(rs.getString(9)), new ReimbursementType(rs.getString(10))));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            log.error(e);
        }
        return result;
    }

}