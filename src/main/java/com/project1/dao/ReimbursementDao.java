package com.project1.dao;

import com.project1.model.Reimbursement;
import com.project1.model.ReimbursementType;
import com.project1.model.User;

public interface ReimbursementDao {
	public boolean reimbursementRequest(int amount, String description, byte[] receipt, User user, ReimbursementType type);
	public boolean acceptReimbursementRequest(Reimbursement reimbursement, User user);
	public boolean denyReimbursementRequest(Reimbursement reimbursement, User user);
}
