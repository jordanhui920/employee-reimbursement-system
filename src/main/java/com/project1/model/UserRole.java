package com.project1.model;

public class UserRole {
	private int role;
	
	public UserRole() {
		
	}
	
	public UserRole(String role) {
		setRole(role);
	}

	public void setRole(int role) {
		this.role = role;
	}
	
	public void setRole(String role) {
		if (role.equals("Employee")) {
			this.role = 1;
		}
		else if (role.equals("Manager")) {
			this.role = 2;
		}
		else
			this.role = -1;
	}

	public String getRole() {
		switch (this.role) {
			case 1: return "Employee";
			case 2: return "Manager";
			default: return "";
		}
	}
}
