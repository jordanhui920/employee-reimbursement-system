package com.project1.model;

public class Pair<T1, T2> {	
	T1 obj1;
	T2 obj2;
	
	public Pair(T1 o1, T2 o2) {
		this.obj1 = o1;
		this.obj2 = o2;
	}
	
	public T1 getKey() {
		return obj1;
	}
	
	public T2 getValue() {
		return obj2;
	}
}
