package com.project1.model;

import java.sql.Timestamp;
import java.util.Arrays;

public class Reimbursement {
    int reimb_id;
    int reimb_amount;
    Timestamp reimb_submitted;
    Timestamp reimb_resolved;
    String reimb_description;
    byte[] receipt;
    String reimb_author;
    String reimb_resolver;
    ReimbursementStatus status;
    ReimbursementType type;

    public Reimbursement() {
        super();
    }

    public Reimbursement(int reimb_id) {
        super();
        this.reimb_id = reimb_id;
    }

    public Reimbursement(int reimb_id, int reimb_amount, Timestamp reimb_submitted, Timestamp reimb_resolved,
        String reimb_description, byte[] receipt, String reimb_author, String reimb_resolver,
        ReimbursementStatus status, ReimbursementType type) {
        super();
        this.reimb_id = reimb_id;
        this.reimb_amount = reimb_amount;
        this.reimb_submitted = reimb_submitted;
        this.reimb_resolved = reimb_resolved;
        this.reimb_description = reimb_description;
        this.receipt = receipt;
        this.reimb_author = reimb_author;
        this.reimb_resolver = reimb_resolver;
        this.status = status;
        this.type = type;
    }
    public int getReimb_amount() {
        return reimb_amount;
    }
    public void setReimb_amount(int reimb_amount) {
        this.reimb_amount = reimb_amount;
    }
    public Timestamp getReimb_resolved() {
        return reimb_resolved;
    }
    public void setReimb_resolved(Timestamp reimb_resolved) {
        this.reimb_resolved = reimb_resolved;
    }
    public String getReimb_description() {
        return reimb_description;
    }
    public void setReimb_description(String reimb_description) {
        this.reimb_description = reimb_description;
    }
    public byte[] getReceipt() {
        return receipt;
    }
    public void setReceipt(byte[] receipt) {
        this.receipt = receipt;
    }
    public String getReimb_author() {
        return reimb_author;
    }
    public void setReimb_author(String reimb_author) {
        this.reimb_author = reimb_author;
    }
    public ReimbursementStatus getStatus() {
        return status;
    }
    public void setStatus(ReimbursementStatus status) {
        this.status = status;
    }
    public ReimbursementType getType() {
        return type;
    }
    public void setType(ReimbursementType type) {
        this.type = type;
    }
    public int getReimb_id() {
        return reimb_id;
    }
    public Timestamp getReimb_submitted() {
        return reimb_submitted;
    }
    public String getReimb_resolver() {
        return reimb_resolver;
    }

    @Override
    public String toString() {
        return "Reimbursement [reimb_id=" + reimb_id + ", reimb_amount=" + reimb_amount + ", reimb_submitted=" +
            reimb_submitted + ", reimb_resolved=" + reimb_resolved + ", reimb_description=" + reimb_description +
            ", receipt=" + Arrays.toString(receipt) + ", reimb_author=" + reimb_author + ", reimb_resolver=" +
            reimb_resolver + ", status=" + status + ", type=" + type + "]";
    }


}