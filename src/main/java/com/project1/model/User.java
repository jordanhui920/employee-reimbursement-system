package com.project1.model;

public class User {
	int ers_users_id;
	String ers_username;
	String ers_password;
	String user_first_name;
	String user_last_name;
	String user_email;
	UserRole role;
	
	public User() {
		super();
	}
	
	public User(int ers_users_id, String ers_username, String ers_password, String user_first_name,
			String user_last_name, String user_email, UserRole role) {
		super();
		this.ers_users_id = ers_users_id;
		this.ers_username = ers_username;
		this.ers_password = ers_password;
		this.user_first_name = user_first_name;
		this.user_last_name = user_last_name;
		this.user_email = user_email;
		this.role = role;
	}
	public int getErs_users_id() {
		return ers_users_id;
	}
	public void setErs_users_id(int ers_users_id) {
		this.ers_users_id = ers_users_id;
	}
	public String getErs_password() {
		return ers_password;
	}
	public void setErs_password(String ers_password) {
		this.ers_password = ers_password;
	}
	public String getUser_first_name() {
		return user_first_name;
	}
	public void setUser_first_name(String user_first_name) {
		this.user_first_name = user_first_name;
	}
	public String getUser_last_name() {
		return user_last_name;
	}
	public void setUser_last_name(String user_last_name) {
		this.user_last_name = user_last_name;
	}
	public String getUser_email() {
		return user_email;
	}
	public void setUser_email(String user_email) {
		this.user_email = user_email;
	}
	public UserRole getRole() {
		return role;
	}
	public void setRole(UserRole role) {
		this.role = role;
	}
	public String getErs_username() {
		return ers_username;
	}
	public String getFullName() { //first last
		return user_first_name + "  " + user_last_name;
	}
	@Override
	public String toString() {
		return "User [ers_users_id=" + ers_users_id + ", ers_username=" + ers_username + ", ers_password="
				+ ers_password + ", user_first_name=" + user_first_name + ", user_last_name=" + user_last_name
				+ ", user_email=" + user_email + ", role=" + role + "]";
	}
	@Override
	public boolean equals(Object o) {
		System.out.println("overrided");
		if (o instanceof User) {
			System.out.println("overrided in");
			User u = (User)o;
			if (this.ers_users_id == u.getErs_users_id() && this.ers_username.equals(getErs_username())
					&& this.user_first_name.equals(getUser_first_name()) && this.user_last_name.equals(u.getUser_last_name())
					&& this.user_email.equals(getUser_email()))
				return true;
		}
		return false;
	}
}
