package com.project1.model;

public class ReimbursementStatus {
	private int status;
	
	public ReimbursementStatus() {
		
	}
	
	public ReimbursementStatus(String role) {
		setStatus(role);
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
	public void setStatus(String status) {
		if (status.equals("Pending")) {
			this.status = 1;
		}
		else if (status.equals("Approved")) {
			this.status = 2;
		}
		else if (status.equals("Denied")) {
			this.status = 3;
		}
		else
			this.status = -1;
	}

	public String getStatus() {
		switch (this.status) {
			case 1: return "Pending";
			case 2: return "Approved";
			case 3: return "Denied";
			default: return "";
		}
	}
}

