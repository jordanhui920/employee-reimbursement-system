package com.project1.model;

public class ReimbursementType {
	private int type;
	
	public ReimbursementType() {
		
	}
	
	public ReimbursementType(int type) {
		setType(type);
	}
	
	public ReimbursementType(String type) {
		setType(type);
	}

	public void setType(int type) {
		this.type = type;
	}
	
	public void setType(String type) {
		if (type.equals("Lodging")) {
			this.type = 1;
		}
		else if (type.equals("Travel")) {
			this.type = 2;
		}
		else if (type.equals("Food")) {
			this.type = 3;
		}
		else if (type.equals("Other")) {
			this.type = 4;
		}
		else
			this.type = -1;
	}

	public String getType() {
		switch (this.type) {
			case 1: return "Lodging";
			case 2: return "Travel";
			case 3: return "Food";
			case 4: return "Other";
			default: return "";
		}
	}

	public int getIntType() {
		return this.type;
	}
}
