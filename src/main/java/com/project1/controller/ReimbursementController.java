package com.project1.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import com.project1.model.Reimbursement;
import com.project1.model.ReimbursementType;
import com.project1.model.User;
import com.project1.service.ReimbursementService;

public class ReimbursementController {
    static ReimbursementService reimbursementServ = new ReimbursementService();
    private static Logger log = Logger.getLogger(ReimbursementController.class);

    public static boolean newReimbursementRequest(HttpServletRequest req, HttpServletResponse res) {
        User user = (User) req.getSession().getAttribute("currentUser");
        log.debug("in control newReimbursementRequest");
        if (user == null) {
            try {
                res.sendRedirect("html/index.html");
                return false;
            } catch (IOException e) {
                e.printStackTrace();
                log.error(e);
            }
        }
        //int amount, String description, byte[] receipt, User user, ReimbursementType type
        //eq.getParameter("username") 
        //null is for receipt if i want to store it later

        byte[] bytes = null;

        try {
            Part filePart = req.getPart("file");
            InputStream filecontents = filePart.getInputStream();
            bytes = IOUtils.toByteArray(filecontents);
        } catch (ServletException e) {
            log.error(e);
            e.printStackTrace();
        } catch (IOException e) {
            log.error(e);
            e.printStackTrace();
        }
        boolean success = reimbursementServ.newReimbursementRequest(req.getParameter("amount"),
            req.getParameter("description"), bytes, user, new ReimbursementType(req.getParameter("type")));
        try {
            res.sendRedirect("html/employeepage.html");
        } catch (IOException e) {
            log.error(e);
            e.printStackTrace();
        }
        return success;
    }

    public static List < Reimbursement > getPendingReimbursements(HttpServletRequest req, HttpServletResponse res) {
        User user = (User) req.getSession().getAttribute("currentUser");
        log.debug("in control getPendingReimbursements");
        if (user == null) {
            try {
                res.sendRedirect("html/index.html");
                return null;
            } catch (IOException e) {
                log.error(e);
                e.printStackTrace();
            }
        }
        return reimbursementServ.getPendingReimbursement();
    }

    public static List < Reimbursement > getAcceptedReimbursements(HttpServletRequest req, HttpServletResponse res) {
        User user = (User) req.getSession().getAttribute("currentUser");
        log.debug("in control getAcceptedReimbursement");
        if (user == null) {
            try {
                res.sendRedirect("html/index.html");
                return null;
            } catch (IOException e) {
                log.error(e);
                e.printStackTrace();
            }
        }
        return reimbursementServ.getAcceptedReimbursement();
    }

    public static List < Reimbursement > getDeniedReimbursements(HttpServletRequest req, HttpServletResponse res) {
        User user = (User) req.getSession().getAttribute("currentUser");
        log.debug("in control getDeniedReimbursements");
        if (user == null) {
            try {
                res.sendRedirect("html/index.html");
                return null;
            } catch (IOException e) {
                log.error(e);
                e.printStackTrace();
            }
        }
        return reimbursementServ.getDeniedReimbursement();
    }

    public static boolean approveReimbursement(HttpServletRequest req, HttpServletResponse res) {
        User user = (User) req.getSession().getAttribute("currentUser");
        log.debug("in control approvereimbursement");
        if (user == null) {
            try {
                res.sendRedirect("html/index.html");
                return false;
            } catch (IOException e) {
                log.error(e);
                e.printStackTrace();
            }
        }
        try {
        	res.sendRedirect("html/managerpage.html");
        } catch (IOException e) {
            log.error(e);
            e.printStackTrace();
        }
        return reimbursementServ.approveReimbursement(req.getParameter("approveNumber"), user);
    }

    public static boolean denyReimbursement(HttpServletRequest req, HttpServletResponse res) {
        User user = (User) req.getSession().getAttribute("currentUser");
        log.debug("in control denyreimbursement");
        if (user == null) {
            try {
                res.sendRedirect("html/index.html");
                return false;
            } catch (IOException e) {
                log.error(e);
                e.printStackTrace();
            }
        }
        try {
        	res.sendRedirect("html/managerpage.html");
        } catch (IOException e) {
            log.error(e);
            e.printStackTrace();
        }
        return reimbursementServ.denyReimbursement(req.getParameter("denyNumber"), user);
    }
}