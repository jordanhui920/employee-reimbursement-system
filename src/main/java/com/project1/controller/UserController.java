package com.project1.controller;

import java.io.IOException;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.project1.email.EmailDaoImpl;
import com.project1.model.Reimbursement;
import com.project1.model.User;
import com.project1.service.UserService;

public class UserController {
    static UserService userServ = new UserService();
    private static Logger log = Logger.getLogger(UserController.class);

    public static String login(HttpServletRequest req) {
        log.debug("in usercontroller login");
        if (!req.getMethod().equals("POST")) {
        	log.debug("usercontroller attempted login with post");
            return "html/unsuccessfullogin.html";
        }

        User user = userServ.verify(req.getParameter("username"), req.getParameter("password"));
        if (user == null)
            return "wrongcreds.change";
        else {
            req.getSession().setAttribute("currentUser", user);
            return "html/home.html";
        }
    }

    public static User getSessionUser(HttpServletRequest req, HttpServletResponse res) throws JsonProcessingException, IOException {
        User user = (User) req.getSession().getAttribute("currentUser");
        //res.getWriter().write(new ObjectMapper().writeValueAsString(user));
        return user;
    }

    public static User registerNewUser(HttpServletRequest req, HttpServletResponse res) throws JsonProcessingException, IOException {
        User user = null;
        if (req.getParameter("sendemail") == null) {
            log.debug("registerNewUser with given password");
        } else {
            log.debug("registerNewUser email temporary password activate");
        }
        if (req.getParameter("sendemail") == null) { //not checked so just make a user with given password
            user = userServ.makeNewUser(req.getParameter("username"), req.getParameter("password"),
                req.getParameter("firstname"), req.getParameter("lastname"), req.getParameter("email"));
        } else { //checkbox checked so send email with password
            EmailDaoImpl a = new EmailDaoImpl();
            String tempPassword = generateRandomAlphaNumericString(64, 128);
            a.sendTemporaryPassword(req.getParameter("email"), tempPassword);
            user = userServ.makeNewUser(req.getParameter("username"), tempPassword,
                req.getParameter("firstname"), req.getParameter("lastname"), req.getParameter("email"));
        }
        return user;
    }

    public static List < Reimbursement > getCustomerReimbursementRequest(HttpServletRequest req, HttpServletResponse res) {
        User user = (User) req.getSession().getAttribute("currentUser");
        log.debug("in controller getCustomerReimbursementRequest");
        if (user == null) {
            try {
                res.sendRedirect("html/index.html");
                return null;
            } catch (IOException e) {
            	log.error(e);
                e.printStackTrace();
            }
        }
        return userServ.getEmployeeReimbursementRequests(user);
    }

    public static List < Reimbursement > getReimbursementRequest(HttpServletRequest req, HttpServletResponse res) {
        User user = (User) req.getSession().getAttribute("currentUser");
        log.debug("in controller getReimbursementRequest");
        if (user == null) {
            try {
                res.sendRedirect("html/index.html");
                return null;
            } catch (IOException e) {
                e.printStackTrace();
                log.error(e);
            }
        }
        return userServ.getReimbursementRequests();
    }

    public static List < Reimbursement > getPendingReimbursement(HttpServletRequest req, HttpServletResponse res) {
        User user = (User) req.getSession().getAttribute("currentUser");
        log.debug("in controller getpendingreimbursement");
        if (user == null) {
            try {
                res.sendRedirect("html/index.html");
                return null;
            } catch (IOException e) {
            	log.error(e);
                e.printStackTrace();
            }
        }
        return userServ.getReimbursementRequests();
    }

    private static String generateRandomAlphaNumericString(int min, int max) {
    	log.debug("generated random alphanumeric string");
        int leftLimit = 48; // number 0
        int rightLimit = 122; // letter 'z'
        int targetStringLength = ThreadLocalRandom.current().nextInt(min, max + 1);
        Random random = new Random();

        String generatedString = random.ints(leftLimit, rightLimit + 1)
            .limit(targetStringLength)
            .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
            .toString();

        return generatedString;
    }
}