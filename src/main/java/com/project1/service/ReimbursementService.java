package com.project1.service;

import java.util.List;

import org.apache.log4j.Logger;

import com.project1.dao.ReimbursementDaoImpl;
import com.project1.model.Reimbursement;
import com.project1.model.ReimbursementType;
import com.project1.model.User;

public class ReimbursementService {
	private ReimbursementDaoImpl reimbursementDao;
    private static Logger log = Logger.getLogger(ReimbursementDaoImpl.class);
	
	public ReimbursementService() {
		reimbursementDao = new ReimbursementDaoImpl();
	}
	
	public ReimbursementService(ReimbursementDaoImpl dao) {
		reimbursementDao = dao;
	}
	
	public boolean  newReimbursementRequest(String amount, String description, byte[] receipt, User user, ReimbursementType type) {
		//int amount, String description, byte[] receipt, User user, ReimbursementType type
		return reimbursementDao.reimbursementRequest(Integer.parseInt(amount), description, receipt, user, type);
	}

	public List<Reimbursement>  getPendingReimbursement() {
		return reimbursementDao.getPendingReimbursements();
	}

	public List<Reimbursement>  getAcceptedReimbursement() {
		return reimbursementDao.getAcceptedReimbursements();
	}

	public List<Reimbursement>  getDeniedReimbursement() {
		return reimbursementDao.getDeniedReimbursements();
	}

	public boolean  approveReimbursement(String approveNumber, User user) {
		log.debug("reimbursement service approving number " + approveNumber);
		return reimbursementDao.acceptReimbursementRequest(new Reimbursement(Integer.parseInt(approveNumber)), user);
	}

	public boolean  denyReimbursement(String denyNumber, User user) {
		log.debug("reimbursement service denying number " + denyNumber);
		return reimbursementDao.denyReimbursementRequest(new Reimbursement(Integer.parseInt(denyNumber)), user);
	}
}
