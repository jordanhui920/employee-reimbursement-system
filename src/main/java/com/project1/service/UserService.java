package com.project1.service;

import java.util.List;

import org.apache.log4j.Logger;

import com.project1.dao.UserDaoImpl;
import com.project1.model.Reimbursement;
import com.project1.model.User;

public class UserService {
	private UserDaoImpl userDao;
    private static Logger log = Logger.getLogger(UserDaoImpl.class);
	
	public UserService() {
		userDao = new UserDaoImpl();
	}
	
	public UserService(UserDaoImpl dao) {
		userDao = dao;
	}
	
	public User verify(String username, String password) {
		log.debug("in userService calling verify");
		return userDao.verifyLogin(username, password);
	}
	
	public User makeNewUser(String username, String password, String fname, String lname, String email) {
		log.debug("in userService calling makenewuser");
		return userDao.makeNewUser(username, password, fname, lname, email);
	}
	
	public List<Reimbursement>  getEmployeeReimbursementRequests(User user) {
		return userDao.getAllUserRequests(user);
	}
	
	public List<Reimbursement>  getReimbursementRequests() {
		return userDao.getAllUserRequests();
	}
}
