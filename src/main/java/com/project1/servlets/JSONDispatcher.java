package com.project1.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.project1.controller.ReimbursementController;
import com.project1.controller.UserController;
import com.project1.email.EmailDaoImpl;
import com.project1.model.Reimbursement;
import com.project1.model.User;

public class JSONDispatcher {
    private static Logger log = Logger.getLogger(EmailDaoImpl.class);
	
	public void process(HttpServletRequest req, HttpServletResponse res)  throws IOException {
		log.debug("jsondispatcher processing " + req.getRequestURI());
		User user;
		user = UserController.getSessionUser(req, res);
		if (user == null) {
			log.info("no active session, getting out");
			res.sendRedirect("html/index.html");
			return;
		}
		switch (req.getRequestURI()) {
			case "/project1/getsessionuser.json":
				log.debug("in getsessionuser");
				res.getWriter().write(new ObjectMapper().writeValueAsString(user));
				//i guess plan on returning to index using js if no session user
				//res.sendRedirect("html/employeepage.html"); 
				break;
			case "/project1/getReimbursementRequest.json":
				log.debug("in getReimbursementRequest.json");
				List<Reimbursement> reimb;
				if (user.getRole().getRole().indexOf("Manager") >= 0)
					reimb = UserController.getReimbursementRequest(req, res);
				else
					reimb = UserController.getCustomerReimbursementRequest(req, res);
				res.getWriter().write(new ObjectMapper().writeValueAsString(reimb));
				break;
			case "/project1/newReimbursementRequest.json":
				log.debug("in newReimbursementRequest.json" + user);
				ReimbursementController.newReimbursementRequest(req, res);
				break;
			case "/project1/getPendingReimbursements.json":
				log.debug("in getpendingtRequest.json" + user);
				List<Reimbursement> preimbursements = ReimbursementController.getPendingReimbursements(req, res);
				res.getWriter().write(new ObjectMapper().writeValueAsString(preimbursements));
				break;
			case "/project1/getAcceptedReimbursements.json":
				log.debug("in getacceptedRequest.json" + user);
				List<Reimbursement> areimbursements = ReimbursementController.getAcceptedReimbursements(req, res);
				res.getWriter().write(new ObjectMapper().writeValueAsString(areimbursements));
				break;
			case "/project1/getDeniedReimbursements.json":
				log.debug("in getdeniedRequest.json" + user);
				List<Reimbursement> dreimbursements = ReimbursementController.getDeniedReimbursements(req, res);
				res.getWriter().write(new ObjectMapper().writeValueAsString(dreimbursements));
				break;
			case "/project1/approveReimbursement.json":
				log.debug("in approve.json" + user);
				ReimbursementController.approveReimbursement(req, res);
				//res.getWriter().write(new ObjectMapper().writeValueAsString(dreimbursements));
				break;
			case "/project1/denyReimbursement.json":
				log.debug("in deny.json" + user);
				ReimbursementController.denyReimbursement(req, res);
				//res.getWriter().write(new ObjectMapper().writeValueAsString(dreimbursements));
				break;
			default:
				log.debug("in jsondispatcher process default");
				res.getWriter().write(new ObjectMapper().writeValueAsString(new User()));
		}
		
	}
}
