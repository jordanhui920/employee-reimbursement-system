package com.project1.servlets;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.project1.controller.UserController;
import com.project1.email.EmailDaoImpl;
import com.project1.model.User;

public class ViewDispatcher {
    private static Logger log = Logger.getLogger(EmailDaoImpl.class);
    
	public String process(HttpServletRequest req, HttpServletResponse res) {
		log.debug("viewdispatcher processing " + req.getRequestURI());
		switch (req.getRequestURI()) {
			case "/project1/login.change":
				UserController.login(req);
				HttpSession session = req.getSession();
				try {
					if (session.getAttribute("currentUser") != null) {
						User user = UserController.getSessionUser(req, res);
						if (user.getRole().getRole().indexOf("Manager") >= 0) {
							res.sendRedirect("html/managerpage.html");
						}
						else {
							res.sendRedirect("html/employeepage.html");
						}
						return "success";
					}
					else {
						res.sendRedirect("html/index.html");
						return "fail";
					}
				}
				catch (IOException e) {
					e.printStackTrace();
					log.error(e);
				}
				return "fail";
			case "/project1/logout.change":
				req.getSession().setAttribute("currentUser", null);
				try {
					res.sendRedirect("html/index.html");
				}
				catch (IOException e) {
					e.printStackTrace();
					log.error(e);
				}
				return "success";
			case "/project1/register.change":
				User newUser = null;
				try {
					newUser =UserController.registerNewUser(req, res);
					res.sendRedirect("html/index.html");
				}
				catch (IOException e) {
					e.printStackTrace();
					log.error(e);
				}
				if (newUser != null)
					return "success";
				else
					return "fail";
			/*
			case "/project1/registerr.change":
				System.out.println(req.getParameter("username"));
				System.out.println(req.getParameter("sendemail"));
				System.out.println(req.getParameter("password"));
				System.out.println(req.getParameter("email"));
				System.out.println(req.getParameter("firstname"));
				System.out.println(req.getParameter("lastname"));
				if (req.getParameter("sendemail") == null) {
					System.out.println("unchecked");
				}
				else {
					System.out.println("checkbox checked");
				}
			    HttpServletRequest httpRequest = (HttpServletRequest) req;
		        Enumeration<String> headerNames = req.getParameterNames();
		        Map<String, String[]> x = req.getParameterMap();
		        for (Map.Entry<String, String[]> a : req.getParameterMap().entrySet()) {
		        	System.out.println(a.getKey() + " " + a.getValue());
		        }

			    if (headerNames != null) {
			            while (headerNames.hasMoreElements()) {
			                    System.out.println("Header: " + httpRequest.getHeader(headerNames.nextElement()));
			            }
			    }
				try {
					newUser =UserController.registerNewUser(req, res);
					res.sendRedirect("html/index.html");
				}
				catch (IOException e) {
					e.printStackTrace();
				}
				return "fail";
			*/
			default:
				log.debug("viewdispatcher process defaulted with " + req.getRequestURI());
				return "html/index.html";
		}
	}
}
