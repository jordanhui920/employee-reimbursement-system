package com.project1.email;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;

public class EmailDaoImpl {
    ClassLoader classloader = getClass().getClassLoader();
    InputStream is;
    Properties p = new Properties();
    private static Logger log = Logger.getLogger(EmailDaoImpl.class);

    public EmailDaoImpl() {
    	log.debug("emaildao obj instantiated");
        is = classloader.getResourceAsStream("connection.properties");
        try {
            p.load(is);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendTemporaryPassword(String email, String tempPassword) {
    	log.debug("emaildao attempt send email");
        String to = email;

        final String from = p.getProperty("senderEmail");

        String host = "smtp.gmail.com";

        final String password = p.getProperty("emailPassword");

        Properties properties = System.getProperties();

        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port", 465);
        properties.put("mail.smtp.user", from);
        properties.put("mail.smtp.password", password);
        properties.put("mail.smtp.socketFactory.class",
            "javax.net.ssl.SSLSocketFactory");
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");

        // Get the default Session object.
        Session session = Session.getDefaultInstance(properties, new javax.mail.Authenticator() {
            protected PasswordAuthentication
            getPasswordAuthentication() {
                return new PasswordAuthentication(from, password);
            }
        });

        try {
            // Create a default MimeMessage object.
            MimeMessage message = new MimeMessage(session);

            // Set From: header field of the header.
            message.setFrom(new InternetAddress(from));

            // Set To: header field of the header.
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));

            // Set Subject: header field
            message.setSubject("Employee Reimbursement Password");

            // Now set the actual message
            message.setText("Your unhashed password has been set to " + tempPassword);

            // Send message
            Transport transport = session.getTransport("smtp");
            transport.connect(host, from, password);
            Transport.send(message);
            log.info("Sent message successfully....");
        } catch (MessagingException e) {
            e.printStackTrace();
            log.error(e);
        }
    }
}