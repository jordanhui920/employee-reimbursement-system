create table ers_reimbursement_status( -- Pending, Approved, Denied
  reimb_status_id int not null primary key,
  reimb_status varchar(10) not null
);

create table ers_reimbursement_type( -- Lodging, Travel, Food, and Other
  reimb_type_id int not null primary key,
  reimb_type varchar(10) not null
);

create table ers_user_roles( -- Employee and Finance Manager
  ers_user_role_id int not null primary key,
  user_role varchar(10) not null
);

create table ers_users(
  ers_users_id int not null primary key generated always as identity,
  ers_username varchar(50) unique not null,
  ers_password varchar(50) not null,
  user_first_name varchar(100) not null,
  user_last_name varchar(100) not null,
  user_email varchar(150) unique not null,
  user_role_id int not null references ers_user_roles(ers_user_role_id)
);

create table ers_reimbursement(
  reimb_id int not null primary key generated always as identity,
  reimb_amount int not null,
  reimb_submitted timestamp not null,
  reimb_resolved timestamp,
  reimb_description varchar(250),
  reimb_receipt bytea,
  reimb_author int not null references ers_users(ers_users_id),
  reimb_resolver int references ers_users(ers_users_id),
  reimb_status_id int not null references ers_reimbursement_status(reimb_status_id),
  reimb_type_id int not null references ers_reimbursement_type(reimb_type_id)
);

select * from  ers_reimbursement_status;
select * from  ers_reimbursement_type;
select * from  ers_user_roles;
select * from  ers_users;
select * from  ers_reimbursement;

--below are hard coded values that must be inserted after inserting tables

insert into ers_reimbursement_status(reimb_status_id, reimb_status) values (1, 'Pending');
insert into ers_reimbursement_status(reimb_status_id, reimb_status) values (2, 'Approved');
insert into ers_reimbursement_status(reimb_status_id, reimb_status) values (3, 'Denied');
insert into ers_reimbursement_type(reimb_type_id, reimb_type) values (1, 'Lodging');
insert into ers_reimbursement_type(reimb_type_id, reimb_type) values (2, 'Travel');
insert into ers_reimbursement_type(reimb_type_id, reimb_type) values (3, 'Food');
insert into ers_reimbursement_type(reimb_type_id, reimb_type) values (4, 'Other');
insert into ers_user_roles(ers_user_role_id, user_role) values (1, 'Employee');
insert into ers_user_roles(ers_user_role_id, user_role) values (2, 'Manager');

-- always inserts an employee, no managers
create or replace function insert_new_user(p_username varchar(50), p_password varchar(50), p_fname varchar(100), p_lname varchar(100), p_email varchar(150))
returns int as $$ -- returns the id of the inserted user. returns -1 if failed due to username or email not being unique
begin 
	if exists (select * from ers_users u where u.ers_username = p_username) then
		return -1;
	end if;
	if exists (select * from ers_users u where u.user_email = p_email) then
		return -1;
	end if;
	insert into ers_users (ers_username, ers_password, user_first_name, user_last_name, user_email, user_role_id) 
		values (p_username, p_password, p_fname, p_lname, p_email,1);
	return lastval();
end
$$ language 'plpgsql';
-- DROP FUNCTION insert_new_user;

-- verify login and returns an empty table on failure, and non-empty user table on success
create or replace function verify_login(p_username varchar(50), p_password varchar(50))
returns table (id int, username varchar(50), password varchar(50), f_name varchar(100),
	l_name varchar(100), email varchar(150),user_role varchar(10)) as $$
begin 
	return query
	select u.ers_users_id, u.ers_username, u.ers_password, u.user_first_name, u.user_last_name, u.user_email, r.user_role 
		from ers_users u left join ers_user_roles r on u.user_role_id = r.ers_user_role_id where u.ers_username = p_username and u.ers_password = p_password;
end
$$ language 'plpgsql';
-- DROP FUNCTION verify_login;

/* i gave up on these functions. 
create or replace function get_reimb_type_int(p_reimb_type varchar(10))
returns int as $$
begin 
	  return max(*)
	   from ers_reimbursement_type ert
	   where ert.reimb_type = p_reimb_type;
end; 
$$  language 'plpgsql';

create or replace function get_pending_status_int()
returns int as $$
begin 
	   select *
	   from ers_reimbursement_status ers
	   where ers.reimb_status = "Pending";
end; 
$$  language 'plpgsql';
*/

-- inputs a reimbursement request
create or replace function reimbursement_request(p_amount int, p_description varchar(250), p_receipt bytea, p_id int, p_reimb_type int)
returns boolean as $$
begin 
	if not exists (select * from ers_reimbursement_type r where r.reimb_type_id = p_reimb_type) then
		return false;
	end if;
	
	if not exists (select * from ers_users u where u.ers_users_id = p_id) then
		return false;
	end if;
	
	-- of the 3 foreign keys, ers_status will always be set to pending. so we only need to check validity of the type and user_id to see if they exist

	-- below i tried to lookup values in lookup table so passing values wouldn't be so strict in java but i couldn't figure out how to do it
	--reimb_type  int = get_reimb_type_int(p_reimb_type);
	--declare reimb_status int = get_reimb_type_int();

	insert into ers_reimbursement (reimb_amount, reimb_submitted, reimb_description, reimb_receipt, reimb_author, reimb_status_id, reimb_type_id)
		values (p_amount, CURRENT_TIMESTAMP, p_description, p_receipt, p_id, 1, p_reimb_type);
	
	return true;
end
$$ language 'plpgsql';
-- DROP FUNCTION reimbursement_request;

--accept reimbursement
create or replace function accept_reimbursement_request(p_reimb_req_id int, p_manager_id int)
returns boolean as $$
begin 
	if not exists (select * from ers_reimbursement er where er.reimb_id = p_reimb_req_id and er.reimb_status_id = 1) then -- make sure reimbursement id exists
		return false;
	end if;
	
	if not exists (select * from ers_users u where u.ers_users_id = p_manager_id and u.user_role_id = 2) then --magic number 2 = manager, making sure user is manager
		return false;
	end if;

	update ers_reimbursement set reimb_resolved = CURRENT_TIMESTAMP, reimb_resolver = p_manager_id, reimb_status_id = 2 where reimb_id = p_reimb_req_id;
	
	return true;
end
$$ language 'plpgsql';
-- DROP FUNCTION accept_reimbursement_request;


--deny reimbursement
create or replace function deny_reimbursement_request(p_reimb_req_id int, p_manager_id int)
returns boolean as $$
begin 
	if not exists (select * from ers_reimbursement er where er.reimb_id = p_reimb_req_id and er.reimb_status_id = 1) then -- make sure reimbursement id exists
		return false;
	end if;
	
	if not exists (select * from ers_users u where u.ers_users_id = p_manager_id and u.user_role_id = 2) then --magic number 2 = manager, making sure user is manager
		return false;
	end if;

	update ers_reimbursement set reimb_resolved = CURRENT_TIMESTAMP, reimb_resolver = p_manager_id, reimb_status_id = 3 where reimb_id = p_reimb_req_id;
	
	return true;
end
$$ language 'plpgsql';
-- DROP FUNCTION deny_reimbursement_request;

-- get all of the requests by a user
create or replace function get_all_user_requests(p_id int)
returns table (id int, reimb_amount int, reimb_submitted timestamp, reimb_resolved timestamp, reimb_description varchar(250), 
	reimb_receipt bytea, reimb_resolver_fname varchar(100), reimb_resolver_lname varchar(100), reimb_status_id varchar(10), reimb_type varchar(10)) as $$
begin 
	return query
	select r.reimb_id, r.reimb_amount, r.reimb_submitted, r.reimb_resolved, r.reimb_description, r.reimb_receipt, u.user_first_name, u.user_last_name, s.reimb_status, t.reimb_type
		from ers_reimbursement r left join ers_users u on r.reimb_resolver = u.ers_users_id left join ers_reimbursement_status s on s.reimb_status_id = r.reimb_status_id left join ers_reimbursement_type t on t.reimb_type_id = r.reimb_type_id where r.reimb_author = p_id;
end
$$ language 'plpgsql';
-- DROP FUNCTION get_all_user_requests(p_id int);

-- get all of the requests by every employeee
create or replace function get_all_user_requests()
returns table (id int, reimb_amount int, reimb_submitted timestamp, reimb_resolved timestamp, reimb_description varchar(250), 
	reimb_receipt bytea, reimb_resolver_fname varchar(100), reimb_resolver_lname varchar(100), reimb_status_id varchar(10), reimb_type varchar(10), reimb_author_fname varchar(100), reimb_author_lname varchar(100)) as $$
begin 
	return query
	select r.reimb_id, r.reimb_amount, r.reimb_submitted, r.reimb_resolved, r.reimb_description, r.reimb_receipt, u.user_first_name, u.user_last_name, s.reimb_status, t.reimb_type, a.user_first_name, a.user_last_name 
		from ers_reimbursement r left join ers_users u on r.reimb_resolver = u.ers_users_id left join ers_reimbursement_status s on s.reimb_status_id = r.reimb_status_id left join ers_reimbursement_type t on t.reimb_type_id = r.reimb_type_id 
		left join ers_user_roles rr on rr.ers_user_role_id = u.user_role_id and rr.user_role = 'Employee'
		left join ers_users a on r.reimb_author = a.ers_users_id;
end
$$ language 'plpgsql';
-- DROP FUNCTION get_all_user_requests();

-- insert a manager with username "admin" and password "1"
insert into ers_users (ers_username, ers_password, user_first_name, user_last_name, user_email, user_role_id)  values ('admin', '6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c0', 'managerfirstname', 'managerlastname', 'adminemail',2);

select * from  ers_reimbursement_status;
select * from  ers_reimbursement_type;
select * from  ers_user_roles;
select * from  ers_users;
select * from  ers_reimbursement;
/*
select r.reimb_id, r.reimb_amount, r.reimb_submitted, r.reimb_resolved, r.reimb_description, r.reimb_receipt, u.user_first_name, 
		u.user_last_name, s.reimb_status, t.reimb_type, a.user_first_name, a.user_last_name  
		from ers_reimbursement r
		left join ers_users u on r.reimb_resolver = u.ers_users_id
		left join ers_users a on r.reimb_author = a.ers_users_id
		left join ers_reimbursement_type t on t.reimb_type_id = r.reimb_type_id
		left join ers_reimbursement_status s on s.reimb_status_id =r.reimb_status_id
		where r.reimb_status_id = 1;
		*/
